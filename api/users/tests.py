import json

from django.urls import reverse
from rest_framework import status
from rest_framework.authtoken.models import Token
from rest_framework.test import APITestCase

from .models import Profile
from .serializers import UserSerializer

class RegistrationTestCase(APITestCase):

    def test_registration(self):
        data = {
            "email": "testcase@localhost.com",
            "password1": "some_password123",
            "password2": "some_password123",
        }

        response = self.client.post("/rest-auth/registration/", data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

class UserProfileTestCase(APITestCase):

    def userData(self):
        return {
            "email": "test@testcase.com",
            "password": "this_is_my_pw",
            "first_name": "Jannes",
            "last_name": "Lampe",
            "date_of_birth": "1997-01-01",
            "address": {
                "street": "Dalinghausen",
                "house_number": "15a",
                "zip": 49401,
                "city": "Damme"
            }
        }

    def setUp(self):
        self.profile = Profile.objects.create_user(email=self.userData()["email"],
                                                   password=self.userData()["password"])
        self.token = Token.objects.create(user=self.profile)
        self.api_authentication()

    def api_authentication(self):
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + str(self.token))

    
    def test_user_authenticated(self):
        response = self.client.get("/rest-auth/user/")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['email'], self.userData()["email"])

    def test_user_un_authenticated(self):
        self.client.force_authenticate(user=None)
        response = self.client.get("/rest-auth/user/")
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_user_update(self):
        response = self.client.put("/rest-auth/user/", data=self.userData(), format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        self.assertEqual(response.data['email'], self.userData()["email"])
        self.assertEqual(response.data['first_name'], self.userData()["first_name"])
        self.assertEqual(response.data['last_name'], self.userData()["last_name"])
        self.assertEqual(response.data['date_of_birth'], self.userData()["date_of_birth"])
        self.assertEqual(response.data['address']['street'], self.userData()["address"]['street'])
        self.assertEqual(response.data['address']['house_number'], self.userData()["address"]['house_number'])
        self.assertEqual(response.data['address']['zip'], self.userData()["address"]['zip'])
        self.assertEqual(response.data['address']['city'], self.userData()["address"]['city'])

    def test_user_update_unauthenticated(self):
        self.client.force_authenticate(user=None)
        response = self.client.put("/rest-auth/user/", data=self.userData(), format='json')

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

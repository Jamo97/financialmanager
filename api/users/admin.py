from django.contrib import admin

from users.models import Profile, Address

admin.site.register(Profile)
admin.site.register(Address)

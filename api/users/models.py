from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils.translation import ugettext_lazy as _

from .managers import ProfileManager

# from rest_framework.authentication import TokenAuthentication

class Profile(AbstractUser):
    username = None
    email = models.EmailField(_('email address'), unique=True)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    objects = ProfileManager()

    # TODO: Add Custom Profile Fields
    date_of_birth = models.DateField(blank=True, null=True)

    def __str__(self):
        return self.first_name + " " + self.last_name

class Address(models.Model):
    street = models.CharField(_("street"), max_length=50, blank=True)
    house_number = models.CharField(_("house number"), max_length=10, blank=True)
    zip = models.PositiveIntegerField(_("zip"), blank=True)
    city = models.CharField(_("city"), max_length=50, blank=True)
    
    profile = models.OneToOneField(Profile, verbose_name=_("user profile"), on_delete=models.CASCADE)

    def __str__(self):
        return self.profile.email + " | " + self.street
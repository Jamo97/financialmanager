from rest_framework import serializers

from users.models import Profile, Address

class AddressSerializer(serializers.ModelSerializer):

    class Meta:
        model = Address
        fields = ("street", "house_number", "zip", "city")

class UserSerializer(serializers.ModelSerializer):
    address = AddressSerializer(required=False)

    class Meta:
        model = Profile
        fields = ['id', 'email', 'first_name', 'last_name', 'date_of_birth', "address"]

    def create(self, validated_data):
        address_data = validated_data.pop('address')
        profile = Profile.objects.create(**validated_data)

        address = Address.objects.create(profile=profile, **address_data)
        return profile

    def update(self, instance, validated_data):
        address_data = validated_data.pop('address')

        try:
            address = instance.address
        except Exception as e:
            if str(e) == "Profile has no address.":
                Address.objects.create(profile=instance, **address_data)
            else:
                raise "Ein unbekannter Fehler ist aufgetreten!"
        else:
            for k, v in address_data.items():
                setattr(address, k, v)
            address.save()

        instance.email = validated_data.get('email', instance.email)
        instance.first_name = validated_data.get('first_name', instance.first_name)
        instance.last_name = validated_data.get('last_name', instance.last_name)
        instance.date_of_birth = validated_data.get('date_of_birth', instance.date_of_birth)

        instance.save()
        return instance
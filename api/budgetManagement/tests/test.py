import json

from django.urls import reverse
from rest_framework import status
from rest_framework.authtoken.models import Token
from rest_framework.test import APITestCase
from rest_framework.response import Response

from users.models import Profile

from ..models import Household
from ..serializers import HouseHoldSerializer


# TODO alles als ein ViewSet testen -> https://www.caktusgroup.com/blog/2019/02/01/creating-api-endpoint-django-rest-framework/

class HouseholdViewSetTestCase(APITestCase):
    
    def userData(self):
        return {
            "email": "test@testcase.com",
            "password": "this_is_my_pw",
            "first_name": "Jannes",
            "last_name": "Lampe",
            "date_of_birth": "1997-01-01"
        }

    def member(self):
        return {
            "email": "member@testcase.com",
            "password": "this_is_my_pw",
            "first_name": "I'm",
            "last_name": "Member",
        }

    def setUp(self):
        self.super_user = Profile.objects.create_superuser(email="ImSuper@user.com",
                                                           first_name="super Test",
                                                           last_name="User",
                                                           password="im_the_superest_user")
        self.super_user_token = Token.objects.create(user=self.super_user)

        self.profile = Profile.objects.create_user(email=self.userData()["email"],
                                                   first_name=self.userData()["first_name"],
                                                   last_name=self.userData()["last_name"],
                                                   password=self.userData()["password"])
        self.token = Token.objects.create(user=self.profile)

        self.member = Profile.objects.create_user(email=self.member()["email"],
                                                  first_name=self.member()["first_name"],
                                                  last_name=self.member()["last_name"],
                                                  password=self.member()["password"])
        self.member_token = Token.objects.create(user=self.member)

    def api_authentication(self, token):
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + str(token))

    def create_api_household(self, name, creator):
        household = Household.objects.create(name=name, created_from=creator)
        household.members.add(creator.id)

        return household

    # test CREATE
    def test_household_create(self):
        self.api_authentication(self.token)
    
        data = {
            "name": "Erster Testhaushalt"
        }
        response = self.client.post("/api/household/", data=data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['name'], data["name"])
        self.assertEqual(response.data['created_from'], self.profile.first_name + " " + self.profile.last_name)
        self.assertEqual(len(response.data['members']), 1)
        self.assertEqual(response.data['members'][0], self.profile.id)

    def test_household_create_un_authenticated(self):
        self.client.force_authenticate(user=None)
        response = self.client.post("/api/household/", data={})
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertNotEqual(response.status_code, status.HTTP_201_CREATED)
    
    # Test RETRIEVE 
    def test_household_retrieve(self):
        household = self.create_api_household("Erster Testhaushalt", self.member)
        self.api_authentication(self.member_token)

        response = self.client.get(reverse('household-detail', kwargs={'pk': household.pk}))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["name"], "Erster Testhaushalt")
        self.assertEqual(response.data["created_from"], self.member.first_name + " " + self.member.last_name)
        self.assertEqual(len(response.data["members"]), 1)
    
    def test_household_retrieve_un_authenticated(self):
        household = self.create_api_household("Erster Testhaushalt", self.member)
      
        self.client.force_authenticate(user=None)
        response = self.client.get(reverse('household-detail', kwargs={'pk': household.pk}))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertNotEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['detail'].code, "not_authenticated")
    
    def test_household_retrieve_false_authenticated(self):
        household = self.create_api_household("Erster Testhaushalt", self.member)
        self.api_authentication(self.token)
      
        response = self.client.get(reverse('household-detail', kwargs={'pk': household.pk}))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertNotEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['detail'].code, "permission_denied")

    # Test UPDATE 
    def test_household_update(self):
        household = self.create_api_household("Testhaushalt", self.member)
        self.api_authentication(self.member_token)

        response = self.client.put(reverse('household-detail', kwargs={'pk': household.pk}), 
            data={
                "name": "Updated Testhaushalt",
                "creator": self.member,
                "members": [self.member.id]
            }
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["name"], "Updated Testhaushalt")
    
    def test_household_update_un_authenticated(self):
        household = self.create_api_household("Testhaushalt", self.member)
        self.client.force_authenticate(user=None)

        response = self.client.put(reverse('household-detail', kwargs={'pk': household.pk}), data={})
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertNotEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['detail'].code, "not_authenticated")
    
    def test_household_update_false_authenticated(self):
        household = self.create_api_household("Testhaushalt", self.member)
        self.api_authentication(self.token)
      
        response = self.client.put(reverse('household-detail', kwargs={'pk': household.pk}), data={})
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertNotEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['detail'].code, "permission_denied")

    # Test DELETE 
    def test_household_delete(self):
        household = self.create_api_household("Testhaushalt", self.member)
        household_count = Household.objects.all().count()

        self.api_authentication(self.member_token)

        response = self.client.delete(reverse('household-detail', kwargs={'pk': household.pk}))

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(Household.objects.all().count(), household_count - 1)
    
    def test_household_update_un_authenticated(self):
        household = self.create_api_household("Testhaushalt", self.member)
        household_count = Household.objects.all().count()
        
        self.client.force_authenticate(user=None)

        response = self.client.delete(reverse('household-detail', kwargs={'pk': household.pk}))

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertNotEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(response.data['detail'].code, "not_authenticated")
        self.assertEqual(Household.objects.all().count(), household_count)
    
    def test_household_update_false_authenticated(self):
        household = self.create_api_household("Testhaushalt", self.member)
        household_count = Household.objects.all().count()
        
        self.api_authentication(self.token)

        response = self.client.delete(reverse('household-detail', kwargs={'pk': household.pk}))

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertNotEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(response.data['detail'].code, "permission_denied")
        self.assertEqual(Household.objects.all().count(), household_count)


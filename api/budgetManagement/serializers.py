from rest_framework import serializers

from .models import Household
from users.models import Profile

class HouseHoldSerializer(serializers.ModelSerializer):

    created_from = serializers.StringRelatedField(read_only=True)
    members = serializers.PrimaryKeyRelatedField(many=True, queryset=Profile.objects.all())
    # slug = serializers.SlugField(read_only=True)

    # created_at = serializers.SerializerMethodField()

    class Meta:
        model = Household
        fields = ("id", "name", "created_from", "members")

from rest_framework import permissions

class IsHouseholdMember(permissions.BasePermission):
    
    def has_object_permission(self, request, view, obj):
        if request.user in obj.members.all():
            return True
        return obj.created_from == request.user

class IsHouseholdOwner(permissions.BasePermission):

    def has_object_permission(self, request, view, obj):
        return obj.created_from == request.user
from django.urls import include, path
from rest_framework.routers import DefaultRouter

from .views import HouseholdViewSet

router = DefaultRouter()
router.register(r"household", HouseholdViewSet, basename="household")

urlpatterns = [
    path("", include(router.urls))
]


from rest_framework.mixins import (CreateModelMixin, DestroyModelMixin,
                                   ListModelMixin, RetrieveModelMixin,
                                   UpdateModelMixin)
from rest_framework.permissions import IsAdminUser, IsAuthenticated
from rest_framework.viewsets import GenericViewSet, ViewSet

from .exceptions import NotAllowedToRemoveMember, CreatorCantBeRemoved
from .models import Household
from .permissions import IsHouseholdMember, IsHouseholdOwner
from .serializers import HouseHoldSerializer


class HouseholdViewSet(CreateModelMixin,
                       RetrieveModelMixin,
                       UpdateModelMixin,
                       DestroyModelMixin,
                       ListModelMixin,
                       GenericViewSet):
    queryset = Household.objects.all()
    serializer_class = HouseHoldSerializer
    permission_classes = [IsAuthenticated]

    permission_classes_by_action = {'create': [IsAuthenticated],
                                    'list': [IsAdminUser],
                                    'retrieve': [IsHouseholdMember],
                                    'destroy': [IsHouseholdOwner], #IsAdminUser, 
                                    'update': [IsHouseholdMember]}

    def perform_create(self, serializer):
        created_from = self.request.user
        household = serializer.save(created_from=created_from)
        household.members.add(created_from)

    def perform_update(self, serializer):
        household = self.get_object()
        members = self.get_object().members.all()

        # Prüft ob die Mitglieder Anzahl sich verändert hat und ob dies erlaubt ist
        if members.count() != len(serializer.validated_data.get('members')):
            # Mitglieder Anzahl hat sich verändert
            if self.request.user == household.created_from:
                # prüfen ob er sich selbst gelöscht hat 
                if self.request.user in serializer.validated_data.get('members'):
                    # Darf alle Personen löschen außer sich selbst
                    return super().perform_update(serializer)
                else:
                    raise CreatorCantBeRemoved()
            elif self.request.user != household.created_from:
                if self.request.user in serializer.validated_data.get('members'):
                    raise NotAllowedToRemoveMember()
                elif self.request.user not in serializer.validated_data.get('members') and len(serializer.validated_data.get('members')) == (members.count() -1):
                    print("Er hat nur sich selbst gelöscht!!")
                    return super().perform_update(serializer)
                else:
                    print("Er hat mehr als nur sich gelöscht")
                    raise NotAllowedToRemoveMember()
    
        return super().perform_update(serializer)

    def get_permissions(self):
        try:
            # return permission_classes depending on `action` 
            return [permission() for permission in self.permission_classes_by_action[self.action]]
        except KeyError: 
            # action is not set return default permission_classes
            return [permission() for permission in self.permission_classes]

from django.db import models
from django.utils.translation import ugettext_lazy as _

from users.models import Profile


class Household(models.Model):
    name = models.CharField(_("Household Name"), max_length=50)

    created_from = models.OneToOneField(Profile, verbose_name=_("Creator"), on_delete=models.CASCADE)
    members = models.ManyToManyField(Profile, verbose_name=_("Household members"), related_name="members")

    # slug = models.SlugField(unique=True, default="")
    # created_at = models.DateTimeField(auto_now_add=True)
    # updated_at = models.DateTimeField(auto_now=True)

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        self.slug = slugify(self.name)
        return super().save(force_insert=force_insert, force_update=force_update, using=using, update_fields=update_fields)

    def __str__(self):
        return self.name
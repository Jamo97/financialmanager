from rest_framework.exceptions import APIException


class NotAllowedToRemoveMember(APIException):
    status_code = 403
    default_detail = "You are not allowed to remove a member!"
    default_code = "not_allowed_to_remove_member"

class CreatorCantBeRemoved(APIException):
    status_code = 403
    default_detail = "The Creator can't be removed from members!"
    default_code = "creator_cant_be_removed"